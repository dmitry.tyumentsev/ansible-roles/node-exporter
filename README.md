Nginx role
=========

Installs Nginx on Fedora, Debian/Ubuntu servers. Enables default site.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.nginx
```
